'use strict'


const data = (function (){

    const products = [
        {
            id:1,
            type:"Laptop",
            brand:"HP",
            price: 100000
        },
        {
            id:2,
            type:"Mouse",
            brand:"A4",
            price: 400
        },
        {
            id:3,
            type:"Keyboard",
            brand:"Logitech",
            price: 300
        }
    ]
    
    return products;
})(); // IIFE : Always one time execute

function xyz(arg1){

}

let table = document.createElement('table'); // Virtual DOM

for(let product of data){
    
    let itemIdText = document.createTextNode(product.id);
    let itemIdTd = document.createElement('td');
    itemIdTd.appendChild(itemIdText);

    let itemTypeText = document.createTextNode(product.type);
    let itemTypeTd = document.createElement('td');
    itemTypeTd.appendChild(itemTypeText);

    let itemBrandText = document.createTextNode(product.brand);
    let itemBrandTd = document.createElement('td');
    itemBrandTd.appendChild(itemBrandText);

    let itemPriceText = document.createTextNode(product.price);
    let itemPriceTd = document.createElement('td');
    itemPriceTd.appendChild(itemPriceText);
    
    
    let tr = document.createElement('tr');  // find the reason why
    tr.appendChild(itemIdTd);
    tr.appendChild(itemTypeTd);
    tr.appendChild(itemBrandTd);
    tr.appendChild(itemPriceTd);
    
    
    table.appendChild(tr);
    
}

console.log(table);
let container = document.querySelector("#root")
container.appendChild(table);
