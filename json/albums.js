function getAlbumUserid(userId) {
    let albumUserid = null;

    let albumUseridText = document.createTextNode("UserId:"+userId+",");

    let albumUseridContainer = document.createElement('em');

    albumUseridContainer.appendChild(albumUseridText);

    albumUserid = albumUseridContainer;

    return albumUserid;
}


function getAlbumId(id) {
    let albumId= null;

    
    
    let albumIdText = document.createTextNode("Id:"+id+",");


    let albumIdContainer = document.createElement('span');

    albumIdContainer.appendChild(albumIdText);

    albumId = albumIdContainer;

    return albumId;
}





function getAlbumTitle(title) {
    let albumTitle = null;

    let albumTitleText = document.createTextNode("Title:"+title+".");


    let albumTitleContainer = document.createElement('strong');

    albumTitleContainer.appendChild(albumTitleText);

    albumTitle = albumTitleContainer;

    return albumTitle;
}





function getAlbum(userId,id,title) {

    let album0 = null;
    let albumContainer = document.createElement('div')
    let albumUserid = getAlbumUserid(userId);
    let albumId = getAlbumId(id);
    let albumTitle = getAlbumTitle(title);
    

    albumContainer.appendChild(albumUserid);
    albumContainer.appendChild(albumId);
    albumContainer.appendChild(albumTitle);
    

    album0 =albumContainer;

    return album0;
}

function display(htmllocation, albumsData) {

    let htmlLocation = document.querySelector(htmllocation);
    for (albumData of albumsData) {
        let album = getAlbum(albumData.userId,albumData.id,albumData.title);
        htmlLocation.appendChild(album);
    };

    console.log(htmlLocation)
}

fetch('https://jsonplaceholder.typicode.com/albums')
    .then(response => response.json())
    .then(function (albums) {

        display('#root', albums);
    })