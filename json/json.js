const studentcourse = [
  {
    student_name: "Tanvir Ahamed",
    student_id: "C201901",
    occupation: "student",
    emai_id: "satanvir5@gmail.com",
    course_name: "career course",
    gitlab_id: "Satanvir5"
  },

  {
    student_name: "Farjana Akter Tuli",
    student_id: "C201902",
    occupation: "student",
    emai_id: "Farjanatuli2019@gmail.com",
    course_name: "career course",
    gitlab_id: "Farjanatuli"
  },

  {
    student_name: "Sanjida Sarwar",
    student_id: "C201903",
    occupation: "student",
    emai_id: "sanjida657@yahoo.com",
    course_name: "career course",
    gitlab_id: "tupsi"
  },

  {
    student_name: "Mohammed Shameem",
    student_id: "C202904",
    occupation: "student",
    emai_id: "smd95795@gmail.com",
    course_name: "career course",
    gitlab_id: "shameem985795"
  },

  {
    student_name: "Mokter Hossin",
    student_id: "C201905",
    occupation: "student",
    emai_id: "88mhossain@gmail.com",
    course_name: "career course",
    gitlab_id: "88mhossin"
  },

  {
    student_name: "Khairul bashar",
    student_id: "C201906",
    occupation: "student",
    emai_id: "mdkhairukbashar@gmail.com",
    course_name: "career course",
    gitlab_id: "k.b"
  },

  {
    student_name: "Md.Mahade Hasan",
    student_id: "C201907",
    occupation: " student",
    emai_id: "mahadehasan88@gmail.com",
    course_name: "career course",
    gitlab_id: "mhassain"
  },

  {
    student_name: "Puza Nandy",
    student_id: "C201908",
    occupation: "student",
    emai_id: "pujanandymaitry570@gmail.com",
    course_name: "career course",
    gitlab_id: "PuzaNandy"
  },

  {
    student_name: "Prabir Chowdhury",
    student_id: "C201909",
    occupation: "Student",
    emai_id: "prabir.munna@gmail.com",
    course_name: "career course",
    gitlab_id: "prabi439"
  },

  {
    student_name: "Sukanta Dev",
    student_id: "C201910",
    occupation: "student",
    emai_id: "shlock.dev@gmai.com",
    course_name: "career course",
    gitlab_id: "SukantaDeb"
  },

  {
    student_name: "Rikta Das",
    student_id: "C201911",
    occupation: "student",
    emai_id: "riktapurni@gmail.com",
    course_name: "career course",
    gitlab_id: "riktapurni"
  },

  {
    student_name: "John Taposh Gllbert Rozario",
    student_id: "C201912",
    occupation: "Job",
    emai_id: "jtgr3014@gmail.com",
    course_name: "career course",
    gitlab_id: " @Garincha"
  },

  {
    student_name: "Kawsar Ahmed",
    student_id: "C201913",
    occupation: "student",
    emai_id: "ahmekawsar@gmail.com",
    course_name: "career course",
    gitlab_id: "@kawsarahmed4037"
  }
];
const table = document.getElementById("studentcourse");

for (let studentData of studentcourse) {
  let tr = ""; // find the reason why

  tr += "<tr>";
  tr += "<td>" + studentData.student_name + "</td>";
  tr += "<td>" + studentData.student_id + "</td>";
  tr += "<td>" + studentData.occupation + "</td>";
  tr += "<td>" + studentData.emai_id + "</td>";
  tr += "<td>" + studentData.course_name + "</td>";
  tr += "<td>" + studentData.gitlab_id + "</td>";

  tr += "</tr>";

  table.innerHTML += tr;

  console.log(tr);
}
//console.log(Studentcourse)

let University_list = [
  {
    public: [
      "University of Dhaka ",
      "university of Rajshahi",
      "University of Chittagog",
      "Bangladesh Agricultural University",
      "Jahangirnagar University",
      "khulna University",
      "National University",
      "Islamic University",
      "Jagnnath University",
      "Comilla University",
      "Shahajalal University of Science & Technology",
      "Bangladesh University of Engineering & Technology"
    ]
  },

  {
    private: [
      " International Islamic University",
      "East West University",
      "North South University",
      "Brac University",
      "Independent University Bangladesh",
      "American International University",
      "Eastern University",
      "Northern University Bangladeash",
      "Daffodil International University",
      "Ahasanullah University of Science &Technology",
      "Green University",
      "United International University",
      "Stamford University Bangladesh"
    ]
  }
];

let Friend_list = [
  {
    name: "Tanvir Ahamad Siddiqe",
    Gender: "Male",
    age: 22,
    email: "satanvir5@gmail.com",
    facebook_id: "Tanvir Ahamad Siddiqe"
  },

  {
    name: "Jannatul Nayeem",
    Gender: "Fmale",
    age: 21,
    email: "JannatulNayeem@gmail.com",
    facebook_id: "Jannatul"
  },

  {
    name: "Jarin tasneem",
    Gender: "Fmale",
    age: 22,
    email: "Jarintasneem2016@gmail.com",
    facebook_id: "Jarin Tasneem"
  },

  {
    name: "jannatul Dilshad",
    Gender: "Fmale",
    age: 21,
    email: "Dilshadjahan95@gmail.com",
    facebook_id: "Dilshd Jahan"
  },

  {
    name: "Faysal Bari Chowdhury Shuvo",
    Gender: "Male",
    age: 15,
    email: "Fasalshuvo133@gmail.com",
    facebook_id: "Fasal Shuvo"
  },

  {
    name: "Farhana Akter Akhi",
    Gender: "Fmale",
    age: 20,
    email: "Farhanaakhi2019@gmail.com",
    facebook_id: "Farhana Akhi"
  },

  {
    name: "Sara Binte Karim",
    Gender: "Fmale",
    age: 22,
    email: "Sarakarim2016@gmail.com",
    facebook_id: "Sara Binte Karim"
  }
];

let Country_list = [
  {
    name: "Afghanistan",
    code: "AX"
  },

  {
    name: "Albania",
    code: "AL"
  },

  {
    name: "American Samoa",
    code: "As"
  },

  {
    name: "Argentina",
    code: "AR"
  },

  {
    name: "Australia",
    code: "AU"
  },

  {
    name: "Arub",
    code: "AW"
  }
];

let Color_list = [
  {
    color: "red",
    value: "#f00"
  },

  {
    color: "green",
    value: "#0f0"
  },

  {
    color: "blue",
    value: "#0ff"
  },

  {
    color: "cyan",
    value: "#0ff"
  },

  {
    clour: "magenta",
    value: "#f0f"
  },

  {
    color: "yellow",
    value: "#ff0"
  },

  {
    color: "black",
    value: "#000"
  }
];

let Product_list = [
  {
    id: "5968dd23fc13ae04d9000001",
    product_name: "sildenafil citrate",
    supplier: "Wisozk Inc",
    quantity: 261,
    unit_cost: "$10.47"
  },

  {
    id: "5968dd23fc13ae04d9000002",
    product_name: "Mountain Juniperus ashei",
    supplier: "keebler Hilpert",
    quantity: 292,
    unit_cost: "$8.74"
  },

  {
    id: "5968dd23fc13ae04d9000003",
    product_name: "Dextromathorphan HBr",
    supplier: "Schmitt Weissnat",
    quantity: 211,
    unit_cost: "$20.53"
  }
];

let Test_Data = [
  {
    Id: 1,
    first_name: "Giavani",
    last_name: "Frediani",
    email: "gfrediani1@senate.gov",
    gender: "Male",
    ip_address: "229.179.4.212"
  },

  {
    id: 2,
    first_name: "Noell",
    last_name: "Bea",
    email: "nbea2@imageshack.us",
    gender: "Female",
    ip_address: "180.66.162.255"
  },

  {
    id: 3,
    first_name: "Willard",
    last_name: "Valek",
    email: "wvalek3@vk.com",
    gender: "Male",
    ip_address: "67.76.188.26"
  }
];

let Googlemap_list = [
  {
    markers: [
      {
        name: "Rixos The Palm Dubai",
        position: [25.1212, 55.1535]
      },
      {
        name: "Shangri-La Hotel",
        location: [25.2084, 55.2719]
      },
      {
        name: "Grand Hyatt",
        location: [25.2285, 55.3273]
      }
    ]
  }
];
