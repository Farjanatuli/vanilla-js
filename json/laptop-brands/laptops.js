laptop = [
  {
    model: "Lenevo ideapad 320",
    "graphics card": "Nvidia GEFORCE",
    ram: "8 GB",
    "Hard disk": "2 TB",
    core: "Core i5",
    generation: "8th"
  },
  {
    model: "HP",
    "graphics card": "Nvidia GEFORCE",
    ram: "8 GB",
    "Hard disk": "2 TB",
    core: "Core i5",
    generation: "8th"
  },
  {
    model: "DELL",
    "graphics card": "Nvidia GEFORCE",
    ram: "8 GB",
    "Hard disk": "2 TB",
    core: "Core i5",
    generation: "8th"
  }
];
