const statistics = [

    {
        "fetches": {
            "total": 50,
            "days": [
                {
                    "count": 10,
                    "date": "2019-09-10"
                },
                {
                    "count": 10,
                    "date": "2019-09-09"
                },
                {
                    "count": 10,
                    "date": "2019-09-08"
                },
                {
                    "count": 10,
                    "date": "2019-09-07"
                },
                {
                    "count": 10,
                    "date": "2019-09-06"
                }
            ]
        }
    }
];
