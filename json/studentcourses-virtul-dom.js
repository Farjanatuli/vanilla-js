'use strict'

const data = (function () {
    const studentcourse = [{
            student_name: "Tanvir Ahamed",
            student_id: "C201901",
            occupation: "student",
            email_id: "satanvir5@gmail.com",
            course_name: "career course",
            gitlab_id: "Satanvir5"

        },

        {
            student_name: "Farjana Akter Tuli",
            student_id: "C201902",
            occupation: "student",
            email_id: "Farjanatuli2019@gmail.com",
            course_name: "career course",
            gitlab_id: "Farjanatuli"

        },


        {
            student_name: "Sanjida Sarwar",
            student_id: "C201903",
            occupation: "student",
            email_id: "sanjida657@yahoo.com",
            course_name: "career course",
            gitlab_id: "tupsi"

        },

        {
            student_name: "Mohammed Shameem",
            student_id: "C202904",
            occupation: "student",
            email_id: "smd95795@gmail.com",
            course_name: "career course",
            gitlab_id: "shameem985795"

        },

        {
            student_name: "Mokter Hossin",
            student_id: "C201905",
            occupation: "job",
            email_id: "88mhossain@gmail.com",
            course_name: "career course",
            gitlab_id: "88mhossin"
        },

        {
            student_name: "Khairul bashar",
            student_id: "C201906",
            occupation: "student",
            email_id: "mdkhairukbashar@gmail.com",
            course_name: "career course",
            gitlab_id: "k.b"
        },

        {
            student_name: "Md.Mahade Hasan",
            student_id: "C201907",
            occupation: " student",
            email_id: "mahadehasan88@gmail.com",
            course_name: "career course",
            gitlab_id: "mhassain"

        },

        {
            student_name: "Puza Nandy",
            student_id: "C201908",
            occupation: "student",
            email_id: "pujanandymaitry570@gmail.com",
            course_name: "career course",
            gitlab_id: "PuzaNandy"
        },

        {
            student_name: "Prabir Chowdhury",
            student_id: "C201909",
            occupation: "Student",
            email_id: "prabir.munna@gmail.com",
            course_name: "career course",
            gitlab_id: "prabi439"
        },

        {
            student_name: "Sukanta Dev",
            student_id: "C201910",
            occupation: "student",
            email_id: "shlock.dev@gmai.com",
            course_name: "career course",
            gitlab_id: "SukantaDeb"
        },

        {
            student_name: "Rikta Das",
            student_id: "C201911",
            occupation: "student",
            email_id: "riktapurni@gmail.com",
            course_name: "career course",
            gitlab_id: "riktapurni"

        },

        {
            student_name: "John Taposh Gllbert Rozario",
            student_id: "C201912",
            occupation: "Job",
            email_id: "jtgr3014@gmail.com",
            course_name: "career course",
            gitlab_id: " @Garincha"

        },

        {
            student_name: "Kawsar Ahmed",
            student_id: "C201913",
            occupation: "student",
            email_id: "ahmekawsar@gmail.com",
            course_name: "career course",
            gitlab_id: "@kawsarahmed4037"

        }
    ];
    return studentcourse;

})();

let table = document.createElement('table'); // Virtual DOM
let tr = document.createElement('tr');


let thNameCell = document.createTextNode("student name");
let thName = document.createElement('th');
thName.appendChild(thNameCell);
tr.appendChild(thName);

let thIdCell = document.createTextNode("student id");
let thId = document.createElement('th');
thId.appendChild(thIdCell);
tr.appendChild(thId);


let thOccupationCell = document.createTextNode("occupation");
let thOccupation = document.createElement('th');
thOccupation.appendChild(thOccupationCell);
tr.appendChild(thOccupation);

let thEmailCell = document.createTextNode("email id");
let thEmail = document.createElement('th');
thEmail.appendChild(thEmailCell);
tr.appendChild(thEmail);


let thCourseNameCell = document.createTextNode("course name");

let thCourseName = document.createElement('th');
thCourseName.appendChild(thCourseNameCell);
tr.appendChild(thCourseName);

let thGitIdCell = document.createTextNode("gitlab id");
let thGitId = document.createElement('th');
thGitId.appendChild(thGitIdCell);
tr.appendChild(thGitId);


table.appendChild(tr);

for (let studentcourse of data) {

    let cellName = document.createTextNode(studentcourse.student_name);
    let cellNameTd = document.createElement("td");
    cellNameTd.appendChild(cellName);

    let cellId = document.createTextNode(studentcourse.student_id);
    let cellIdTd = document.createElement("td");
    cellIdTd.appendChild(cellId);


    let cellOccupation = document.createTextNode(studentcourse.occupation);
    let cellOccupationTd = document.createElement("td");
    cellOccupationTd.appendChild(cellOccupation);

    let cellEmailId = document.createTextNode(studentcourse.email_id);
    let cellEmailIdTd = document.createElement("td");
    cellEmailIdTd.appendChild(cellEmailId);

    let cellCourseName = document.createTextNode(studentcourse.course_name);
    let cellCourseNameTd = document.createElement("td");
    cellCourseNameTd.appendChild(cellCourseName);

    let cellGitId = document.createTextNode(studentcourse.gitlab_id);
    let cellGitIdTd = document.createElement("td");
    cellGitIdTd.appendChild(cellGitId);

    let tr = document.createElement('tr');
    tr.appendChild(cellNameTd)
    tr.appendChild(cellIdTd)
    tr.appendChild(cellOccupationTd)
    tr.appendChild(cellEmailIdTd)
    tr.appendChild(cellCourseNameTd)
    tr.appendChild(cellGitIdTd)




    table.appendChild(tr);
}
table.border = 1;
let rootPath = document.querySelector("#root");
rootPath.appendChild(table);
console.log(table);