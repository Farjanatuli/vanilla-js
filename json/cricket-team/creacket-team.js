let cricket_team=[
    {Bangladesh:[
        "Tamim Ikbal","Soumya Sarker",'Najmul Hossain Shanto', 'Liton Das', 'Shakib Al Hasan (c)', 'Mushfiqur Rahim (wk)',
        'Mahmudullah','Mosaddek Hossain','Afif Hossain', 'Aminul Islam',' Mohammad Saifuddin', 
        'Shafiul Islam', 'Mustafizur Rahman','Sabbir Rahman', 'Taijul Islam','Rubel Hossain', 'Mohammad Naim'
    ]},
    {India:['Rohit Sharma', 'Shikhar Dhawan', 'Virat Kohli (c)', 'Shreyas Iyer', 'Rishabh Pant (wk)','Hardik Pandya',
     'Krunal Pandya', 'Ravindra Jadeja', 'Washington Sundar','Deepak Chahar', 'Navdeep Saini', 'Lokesh Rahul',
     'Manish Pandey', 'Rahul Chahar', 'K Khaleel Ahmed']},
  {"South Africa":[
    'Quinton de Kock (c & wk)', 'Reeza Hendricks', 'Temba Bavuma', 'Rassie van der Dussen','David Miller', 'Andile Phehlukwayo',
     'Dwaine Pretorius', 'Bjorn Fortuin', 'Kagiso Rabada', 'Anrich Nortje', 'Tabraiz Shamsi','George Linde', 'Junior Dala', 
     'Beuran Hendricks'
  ]}
]
