function getTodoUserid(userId) {
    let todoUserid = null;

    let todoUseridText = document.createTextNode("UserId:"+userId);

    let todoUseridContainer = document.createElement('h1');

    todoUseridContainer.appendChild(todoUseridText);

    todoUserid = todoUseridContainer;

    return todoUserid;
}


function getTodoId(id) {
    let todoId= null;

    let todoIdText = document.createTextNode("Id:"+id+",");


    let todoIdContainer = document.createElement('h2');

    todoIdContainer.appendChild(todoIdText);

    todoId = todoIdContainer;

    return todoId;
}





function getTodoTitle(title) {
    let todoTitle = null;

    let todoTitleText = document.createTextNode("Title:"+title+",");


    let todoTitleContainer = document.createElement('h3');

    todoTitleContainer.appendChild(todoTitleText);

    todoTitle = todoTitleContainer;

    return todoTitle;
}
function getCheckboxChecker(completed) {
    let checkboxChecker = null;

    let checkboxCheckerText = document.createTextNode("Completed:"+completed+".");


    let checkboxCheckerContainer = document.createElement('strong');

    checkboxCheckerContainer.appendChild(checkboxCheckerText);

    checkboxChecker = checkboxCheckerContainer;

    return checkboxChecker;
}



function getTodo(userId,id,title,completed) {

    let todo0 = null;
    let todoContainer = document.createElement('div');
    let todoUserid = getTodoUserid(userId);
    let todoId = getTodoId(id);
    let todoTitle = getTodoTitle(title);
    let checkboxChecker =getCheckboxChecker(completed)

    todoContainer.appendChild(todoUserid);
    todoContainer.appendChild(todoId);
   todoContainer.appendChild(todoTitle);
    todoContainer.appendChild(checkboxChecker);

    todo0 = todoContainer;

    return todo0;
}

function display(htmllocation, todosData) {

    let htmlLocation = document.querySelector(htmllocation);
    for (todoData of todosData) {
        let todo = getTodo(todoData.userId,todoData.id,todoData.title,todoData.completed);
        htmlLocation.appendChild(todo);
    };

    console.log(htmlLocation)
}

fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(function (todos) {

        display('#root', todos);
    })