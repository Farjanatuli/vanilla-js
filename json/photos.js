
function getphotoId(id) {

    let photoId = null;
    let photoIdText = document.createTextNode("Id:"+id);
    let photoIdContainer = document.createElement('h1');
    photoIdContainer.appendChild(photoIdText);
    photoId = photoIdContainer;
    return photoId;
}

function getphotoTitle(title) {

    let photoTitle = null;
    let photoTitleText = document.createTextNode("Title:"+title+",");
    let photoTitleContainer = document.createElement('h2');
    photoTitleContainer.appendChild(photoTitleText);
    photoTitle = photoTitleContainer;
    return photoTitle;
}

function getphotoUrl(url) {

    let photoUrl = null;
    let photoUrlText = document.createTextNode("Url:"+url+",");
    let photoUrlContainer = document.createElement('strong');
    photoUrlContainer.appendChild(photoUrlText);
    photoUrl = photoUrlContainer;
    return photoUrl;
}

function getphotoThumbnailurl(thumbnailUrl) {
    
    let photoThumbnailurl = null;
    let photoThumbnailurlText = document.createTextNode("Thumbnailurl:"+thumbnailUrl+".");
    let photoThumbnailurlContainer = document.createElement('strong');
    photoThumbnailurlContainer.appendChild(photoThumbnailurlText);
    photoThumbnailurl = photoThumbnailurlContainer;
    return photoThumbnailurl;
}



function getphoto(id, title, url, thumbnailUrl) {
    let photo0 = null;


    let photoContainer = document.createElement('div');
   // let Container = document.createElement('p');

    let photoId = getphotoId(id);
    let photoTitle = getphotoTitle(title);
    let photoUrl = getphotoUrl(url);
    let photoThumbnailurl = getphotoThumbnailurl(thumbnailUrl);


   photoContainer.appendChild(photoId);
   photoContainer .appendChild(photoTitle);
   photoContainer .appendChild(photoUrl);
   photoContainer .appendChild(photoThumbnailurl);
  // photoContainer.appendChild(Container);
    photo0 = photoContainer;

    return photo0;

}
//console.log(getphoto())
function display(htmllocation, photosData) {

    let htmlLocation = document.querySelector(htmllocation);
    for (photoData of photosData) {
        let photo = getphoto(photoData.id, photoData.title, photoData.url, photoData.thumbnailUrl);
        htmlLocation.appendChild(photo);
    };

    console.log(htmlLocation)
}

fetch('https://jsonplaceholder.typicode.com/photos')
    .then(response => response.json())
    .then(function (photos) {

        display('#root', photos);
    })




