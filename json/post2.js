//Pseudo Code of post
//-------------------- this part for post title ------------------------
function getPostTitle(title){
    let postTitle = null;
    // Step 1 : Creating Text Node 
    let postTitleText = document.createTextNode(title);
    
    // Step 2 : Creating Element (h2)
    let postTitleContainer = document.createElement('H1');
    
    // Step 3 : Inserting text node inside Element(h2)
    postTitleContainer.appendChild(postTitleText);
    
    postTitle = postTitleContainer; //(container + content)
    
    return postTitle;
    }
    // ----------------------- this part for post body ----------------------
    function getPostBody(body){
    let postBody = null; // (container + content )
    // step 4 : creating text node 
    let postBodyText = document.createTextNode(body);
    
    // step 5 : creating element (p)
    let postBodyContainer = document.createElement('P');
    
    // step 6 : Inserting text node inside element (p)
    postBodyContainer.appendChild(postBodyText);
    
    postBody = postBodyContainer;
    
    return postBody;
    }
    // ------------------- this part for preparing post -------------------------
    function getPost(title, body){
    let post0 = null;
    
    // step 7 : creating element (div cum container)
    let postContainer = document.createElement('DIV');
    
    let postTitle = getPostTitle(title);
    let postBody = getPostBody(body);
    //step 8 : Inserting post title & post body into container 
    postContainer.appendChild(postTitle);
    postContainer.appendChild(postBody);
    
    post0 = postContainer;
    
    return post0;
    }
    function display(htmllocation, postsData){
    //Targeting html location
    let htmlLocation = document.querySelector(htmllocation);
    for(postData of postsData){
        let post = getPost(postData.title, postData.body);
        htmlLocation.appendChild(post);
    };
    
    console.log(htmlLocation)
    }
    //display('#root', postsData);
    //final step : populate into html tree
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(function(posts){
    //console.log(posts)
    display('#root', posts);
    })
    
    
    
    
    