const doctor_names = [{


    Name: "Dr. Ali Asgar Chowdhury",
    Qualification: "MBBS, FCPS CLINICAL ONCOLOGIST, RESIDENT SURGEON (RADIOTHERAPHY)",
    Organization: "Chittagong Medical College & Hospital",
    Room_No: 414,
    Visiting_Hour: "6pm-8pm"

}, {

    Name: "Dr. M.A Awal",
    Qualification: "MBBS, M.PHIL (RADIOTHERAPY)",
    Designation: "Assistant Professor (Dept. of Radiotherapy)",
    Organization: "Chittagong Medical College & Hospital",
    Room_No: 515,
    Visiting_Hour: "7pm-9pm"

},
{


    Name: "Dr. M. Saleh Uddin Siddique (Ujjal)",
    Qualification: "MBBS, MD (CARDIOLOGY), D-CARD(BSMMU), CLINICAL & INTERVENTIONAL",
    Designation: "Assistant Professor (Cardiology)",
    Organization: "Chittagong Medical College & Hospital",
    Room_No: 418,
    Visiting_Hour: "7.30pm-10pm"

}]