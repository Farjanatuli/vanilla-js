const question = [

    {
        questions: [{
            text: "What does 'API' stand for?",
            answer: "API stands for Application Programming Interface."
        }, {
            text: "What's so good about pragmatic REST?",
            answer: "It's focused on the api consumer, so it makes it easier for developers to contribute to your app library!"
        }]
    }]