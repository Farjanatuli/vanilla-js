const collage_list = [

    {
        chittagong_collage_name=[
            "Bakalia Govt. College",
            "Karnafuli Public College",
            "Banskhali Girls College	",
            "Chittagong College",
            "Govt. Hazi Muhammad Mohsin College	",
            "B A F Shaheen College",
            "South Asian College Chittagong",
            "Agrabad Mohila College",
            "City Commerce College",
            "Halishahar Central College	"
        ]
    },
    {
        dhaka_collage_name=[

            "Notre Dame College",
            "Adamjee Cantonment College",
            "Viqarunnisa Noun Girls School and College",
            "Dhaka City College",
            "Rajuk Uttara Model College",
            "Holy Cross College",
            "Dhaka College",
            "Ideal School and College",
            "Bir Shrestha Nur Mohammad Public College",
            "Dhaka Commerce College"
        ]
    }
];