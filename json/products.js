const products = [
  {
    id: 1,
    type: "Laptop",
    product_name: "HP",
    supplier: "Schmitt Weissnat",
    price: 100000
  },
  {
    id: 2,
    type: "Mouse",
    product_name: "A4",
    supplier: "Schmitt Weissnat",
    price: 400
  },
  {
    id: 3,
    product_name: "logiteh",
    supplier: "Schmitt Weissnat",
    type: "Keyboard",
    price: 300
  },

  {
    id: "222",
    type: "xyz",
    product_name: "sildenafil citrate",
    supplier: "Wisozk Inc",
    quantity: 261,
    price: "$10.47"
  },

  {
    id: "121",
    type: "xyz",
    product_name: "Mountain Juniperus ashei",
    supplier: "keebler Hilpert",
    quantity: 292,
    price: "$8.74"
  },

  {
    id: "123",
    type: "xyz",
    product_name: "Dextromathorphan HBr",
    supplier: "Schmitt Weissnat",
    quantity: 211,
    price: "$20.53"
  }
];

const container = document.getElementById("products-list");

for (let product of products) {
  let tr = ""; // find the reason why

  tr += "<tr>";
  tr += "<td>" + product.id + "</td>";
  tr += "<td>" + product.type + "</td>";
  tr += "<td>" + product.supplier + "</td>";
  tr += "<td>" + product.product_name + "</td>";
  tr += "<td>" + product.price + "</td>";
  tr += "</tr>";

  container.innerHTML += tr;

  console.log(tr);
}
