

function getcommentBody(body) {

    let commentBody = null;
    let commentBodyText = document.createTextNode("Comment:"+body+".");
    let commentBodyContainer = document.createElement('span');
    commentBodyContainer.appendChild(commentBodyText);
    commentBody = commentBodyContainer;
    return commentBody;
}
function getcommentName(name) {

    let commentName = null;
    let commentNameText = document.createTextNode("Name:"+name+",");
    let commentNameContainer = document.createElement('em');
    commentNameContainer.appendChild(commentNameText);
    commentName =commentNameContainer;
    return commentName;
}

function getcommentEmail(email) {

    let commentEmail = null;
    let commentEmailText = document.createTextNode("Email:"+email+",");
    let commentEmailContainer = document.createElement('strong');
    commentEmailContainer.appendChild(commentEmailText);
    commentEmail = commentEmailContainer;

    return commentEmail;
}

function getcomment(name, email, body) {
    let comment0 = null;


    let commentContainer = document.createElement('div');
    let container = document.createElement('p');

    let commentName = getcommentName(name);
    let commentEmail = getcommentEmail(email);
    let commentBody = getcommentBody(body);
   
   
   container.appendChild(commentName);
    container.appendChild(commentEmail);
    container.appendChild(commentBody);
   commentContainer.appendChild(container);
    comment0 = commentContainer;
 
    return comment0;
    
}
console.log(getcomment())
function display(htmllocation, commentsData) {

    let htmlLocation = document.querySelector(htmllocation);
    for (commentData of commentsData) {
        let comment = getcomment(commentData.name, commentData.email, commentData.body);
        htmlLocation.appendChild(comment);
    };

    console.log(htmlLocation)
}

fetch('https://jsonplaceholder.typicode.com/comments')
    .then(response => response.json())
    .then(function (comments) {

        display('#root', comments);
    })




