function getnameUser(name) {

    let nameUser = null;
    let nameUserText = document.createTextNode("Name:" + name + ",");
    let nameUserContainer = document.createElement('p');
    let nameContainer = document.createElement('em');
    nameUserContainer.appendChild(nameUserText);
    nameUserContainer.appendChild(nameContainer);
    nameUser = nameUserContainer;
    return nameUser;
}

function getusersName(username) {

    let usersName = null;
    let usersNameText = document.createTextNode("Usersname:" + username + ",");
    let usersNameContainer = document.createElement('p');
    let usersContainer = document.createElement('span');
    usersNameContainer.appendChild(usersNameText);
    usersNameContainer.appendChild(usersContainer);
    usersName = usersNameContainer;
    return usersName;
}

function getuserEmail(email) {

    let userEmail = null;
    let userEmailText = document.createTextNode("Email:" + email + ",");
    let userEmailContainer = document.createElement('p');
    let userContainer = document.createElement('strong');
    userContainer.appendChild(userEmailText);
    userEmailContainer.appendChild(userContainer);
    userEmail = userEmailContainer;
    return userEmail;
}

function getuserStreet(street) {

    let userStreet = null;
    let userStreetText = document.createTextNode("Address:  Street-" + street + ",");
    let userStreetContainer = document.createElement('strong');
    userStreetContainer.appendChild(userStreetText);
    userStreet = userStreetContainer;
    return userStreet;
}


function getuserSuite(suite) {

    let userSuite = null;
    let userSuiteText = document.createTextNode("Suite-" + suite + ",");
    let userSuiteContainer = document.createElement('strong');
    userSuiteContainer.appendChild(userSuiteText);
    userSuite = userSuiteContainer;
    return userSuite;
}

function getuserCity(city) {

    let userCity = null;
    let userCityText = document.createTextNode("City-" + city + ",");
    let userCityContainer = document.createElement('strong');
    userCityContainer.appendChild(userCityText);
    userCity = userCityContainer;
    return userCity;
}

function getuserZipcode(zipcode) {

    let userZipcode = null;
    let userZipcodeText = document.createTextNode("Zipcode-" + zipcode + ",");
    let userZipcodeContainer = document.createElement('strong');
    userZipcodeContainer.appendChild(userZipcodeText);
    userZipcode = userZipcodeContainer;
    return userZipcode;
}

function getuserLat(lat) {

    let userLat = null;
    let userLatText = document.createTextNode("Geo:Lat-" + lat + ",");
    let userLatContainer = document.createElement('strong');
    userLatContainer.appendChild(userLatText);
    userLat = userLatContainer;
    return userLat;
}

function getuserLng(lng) {

    let userLng = null;
    let userLngText = document.createTextNode("Lng-" + lng + ".");
    let userLngContainer = document.createElement('strong');
    userLngContainer.appendChild(userLngText);
    userLng = userLngContainer;
    return userLng;
}

/*

 function getuserAddress(address) {

    let userAddress = null;

    let userAddressText = document.createTextNode("Address:" + address + ",");
    let userAddressContainer = document.createElement('p');
    userAddressContainer.appendChild(userAddressText);
    userAddress = userAddressContainer;
    return userAddress;

*/

function getuserPhone(phone) {

    let userPhone = null;
    let userPhoneText = document.createTextNode("Phone:" + phone + ",");
    let userPhoneContainer = document.createElement('p');
    let userContainer = document.createElement('strong');
    userPhoneContainer.appendChild(userPhoneText);
    userPhoneContainer.appendChild(userContainer);
    userPhone = userPhoneContainer;
    return userPhone;
}

function getuserWebsite(website) {

    let userWebsite = null;
    let userWebsiteText = document.createTextNode("Website:" + website + ",");
    let userWebsiteContainer = document.createElement('p');
    let userContainer = document.createElement('strong');
    userWebsiteContainer.appendChild(userWebsiteText);
    userWebsiteContainer.appendChild(userContainer);
    userWebsite = userWebsiteContainer;
    return userWebsite;
}

function getcompanyName(name) {

    let companyName = null;
    let companyNameText = document.createTextNode("Company:Name-" + name + ",");
    let companyNameContainer = document.createElement('strong');
    companyNameContainer.appendChild(companyNameText);
    companyName = companyNameContainer;
    return companyName;
}

function getcompanyCatchphrase(catchPhrase) {

    let companyCatchphrase = null;
    let companyCatchphraseText = document.createTextNode("Catch_Pharase-" + catchPhrase + ",");
    let companyCatchphraseContainer = document.createElement('strong');
    companyCatchphraseContainer.appendChild(companyCatchphraseText);
    companyCatchphrase = companyCatchphraseContainer;
    return companyCatchphrase;
}

function getcompanyBs(bs) {

    let companyBs = null;
    let companyBsText = document.createTextNode("Bs-" + bs + ".");
    let companyBsContainer = document.createElement('strong');
    companyBsContainer.appendChild(companyBsText);
    companyBs = companyBsContainer;
    return companyBs;
}

function getuser(name, username, email, street, suite, city, zipcode, lat, lng, phone, website, name, catchPhrase, bs) {
    let user0 = null;


    let userContainer = document.createElement('div');
    //let userContainer = document.createElement('p');

    let nameUser = getnameUser(name);
    let usersName = getusersName(username);
    let userEmail = getuserEmail(email);
    // let userAddress = getuserAddress(address);
    let userStreet = getuserStreet(street);
    let userSuite = getuserSuite(suite);
    let userCity = getuserCity(city);
    let userZipcode = getuserZipcode(zipcode);
    let userLat = getuserLat(lat);
    let userLng = getuserLng(lng);
    let userPhone = getuserPhone(phone);
    let userWebsite = getuserWebsite(website);
    let companyName = getcompanyName(name);
    let companyCatchphrase = getcompanyCatchphrase(catchPhrase);
    let companyBs = getcompanyBs(bs);

    

    userContainer.appendChild(nameUser);
    userContainer.appendChild(usersName);
    userContainer.appendChild(userEmail);
    //userContainer.appendChild(userAddress);
    userContainer.appendChild(userStreet);
    userContainer.appendChild(userSuite);
    userContainer.appendChild(userCity);
    userContainer.appendChild(userZipcode);
    userContainer.appendChild(userLat);
    userContainer.appendChild(userLng);
    userContainer.appendChild(userPhone);
    userContainer.appendChild(userWebsite);
    userContainer.appendChild(companyName);
    userContainer.appendChild(companyCatchphrase);
    userContainer.appendChild(companyBs);
    // userContainer.appendChild(Container);
    user0 = userContainer;

    return user0;

}
console.log(getuser())

function display(htmllocation, usersData) {

    let htmlLocation = document.querySelector(htmllocation);
    for (userData of usersData) {
        let user = getuser(userData.name, userData.username, userData.email, userData.address.street, userData.address.suite, userData.address.city, userData.address.zipcode, userData.address.geo.lat, userData.address.geo.lng, userData.phone, userData.website, userData.company.name, userData.company.catchPhrase, userData.company.bs);
        htmlLocation.appendChild(user);
    };

    console.log(htmlLocation)
}

fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(function (users) {

        display('#root', users);
    })