const restaurants = [
  {
    name: "Good Eats",
    photograph: "Good Eats.jpg",
    address: "Sanmer",
    open: "8am to 12pm",
    rating: 4
  },

  {
    name: "Mission Chinese Food",
    photograph: "1.jpg",
    address: "jaki hosssain road,Gec",
    open: "8am to 12pm",
    rating: 4
  },
  {
    name: "Kutumb Bari Restaurant",
    photograph: "kutumbari.jpg",
    address: "A.K Khan",
    open: "8am to 12pm",
    rating: 5
  },
  {
    name: "Cloud Nine",
    photograph: "Cloud Nine.jpg",
    address: "finlay",
    open: "8am to 12pm",
    rating: 4
  }
];
