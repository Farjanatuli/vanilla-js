function getPostTitle(title) {

    let postTitle = null;
    let postTitleText = document.createTextNode(title);
    let postTitleContainer = document.createElement('H1');
    postTitleContainer.appendChild(postTitleText);
    postTitle = postTitleContainer;
    return postTitle;
}

function getPostBody(body) {

    let postBody = null;
    let postBodyText = document.createTextNode(body);
    let postBodyContainer = document.createElement('P');
    postBodyContainer.appendChild(postBodyText);
    postBody = postBodyContainer;
    return postBody;
}

function getPost(title, body) {

    let post0 = null;
    let postContainer = document.createElement('DIV');
    let postTitle = getPostTitle(title);
    let postBody = getPostBody(body);
    postContainer.appendChild(postTitle);
    postContainer.appendChild(postBody);
    post0 = postContainer;

    return post0;
}

function display(htmllocation, postsData) {

    let htmlLocation = document.querySelector(htmllocation);
    for (postData of postsData) {
        let post = getPost(postData.title, postData.body);
        htmlLocation.appendChild(post);
    };

    console.log(htmlLocation)
}

fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(function (posts) {

        display('#root', posts);
    })