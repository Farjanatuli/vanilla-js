const collage_data = [
    {

        name: "Notre Dame College",
        address: "1, Arambagh, Motijheel, Dhaka",
        number_of_students: "3085",
        established: "1949",
        phone: "01629-955654"
    },
    {
        name: "Adamjee Cantonment College",
        address: "Shahid Sarani, Dhaka",
        number_of_students: "2967",
        established: "1960",
        phone: "02-8872446"


    },
    {
        name: "Viqarunnisa Noon School and College",
        address: "101 New Market – Peelkhana Road, Dhaka",
        number_of_students: "3789",
        established: "1952",
        phone: "02-58310500"


    },
    {
        name: "Rajuk Uttara Model College",
        address: "Sector: 6, Dhaka 1230",
        number_of_students: "4924",
        established: "1994",
        phone: "02-58952780"

    }
]